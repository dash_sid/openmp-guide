/* OpenMP Performance - Loop Vectorization without Reduction */

Numerical integration with n=1000000000
 Step        Time, ms        GSteps/s        Accuracy
    1        8281.995           0.121            -nan*
    2        8286.144           0.121       2.501e+00*
    3        8275.298           0.121       4.756e+00*
    4        8282.492           0.121       9.555e+00
    5        8265.720           0.121       2.175e+01
    6        8270.605           0.121       6.690e+01
    7        8276.049           0.121       5.155e+02
    8        8264.914           0.121       2.606e+03
    9        8310.214           0.120       1.518e+02
   10        8285.897           0.121       5.746e+01
-----------------------------------------------------
Average performance:
             8279.4+-14.6        0.1+-0.0
-----------------------------------------------------
* - warm-up, not included in average


real	1m22.803s
user	10m13.814s
sys	0m0.257s


/* OpenMP Performance - Loop Vectorization with Reduction */

Numerical integration with n=1000000000
 Step        Time, ms        GSteps/s        Accuracy
    1        2205.840           0.453            -nan*
    2        2207.359           0.453       1.393e+01*
    3        2204.261           0.454       4.384e+00*
    4        2202.011           0.454       2.877e+00
    5        2214.810           0.452       2.289e+00
    6        2201.850           0.454       1.979e+00
    7        2212.286           0.452       1.789e+00
    8        2209.343           0.453       1.660e+00
    9        2204.505           0.454       1.567e+00
   10        2206.609           0.453       1.497e+00
-----------------------------------------------------
Average performance:
             2207.3+-4.7        0.5+-0.0
-----------------------------------------------------
* - warm-up, not included in average


real	0m22.073s
user	2m54.477s
sys	0m0.089s