/* OpenMP Performance - Loop Vectorization without Reduction */

Numerical integration with n=1000000000
 Step        Time, ms        GSteps/s        Accuracy
    1        3714.276           0.269            -nan*
    2        3761.561           0.266       2.916e+00*
    3        3714.564           0.269       5.595e+00*
    4        3767.891           0.265       1.805e+01
    5        3707.203           0.270       2.599e+01
    6        3710.699           0.269       1.193e+02
    7        3748.697           0.267       2.739e+04
    8        3732.738           0.268       4.106e+02
    9        3707.889           0.270       1.033e+02
   10        3733.298           0.268       2.759e+01
-----------------------------------------------------
Average performance:
             3729.8+-21.3        0.3+-0.0
-----------------------------------------------------
* - warm-up, not included in average


real	0m37.304s
user	4m42.327s
sys	0m0.261s


/* OpenMP Performance - Loop Vectorization with Reduction */

Numerical integration with n=1000000000
 Step        Time, ms        GSteps/s        Accuracy
    1        1728.985           0.578            -nan*
    2        1724.995           0.580       1.393e+01*
    3        1723.907           0.580       4.384e+00*
    4        1724.428           0.580       2.877e+00
    5        1724.063           0.580       2.289e+00
    6        1724.016           0.580       1.979e+00
    7        1724.064           0.580       1.789e+00
    8        1724.010           0.580       1.660e+00
    9        1723.632           0.580       1.567e+00
   10        1723.827           0.580       1.497e+00
-----------------------------------------------------
Average performance:
             1724.0+-0.2        0.6+-0.0
-----------------------------------------------------
* - warm-up, not included in average


real	0m17.251s
user	2m17.019s
sys	0m0.057s
