#ifndef __INCLUDE_LIBRARY_H__
#define __INCLUDE_LIBRARY_H__

#pragma omp declare simd
double BlackBoxFunction(const double x);

double InverseDerivative(const double x);

#endif