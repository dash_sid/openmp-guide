#include "library.h"

double integral(const int n, const double a, const double b) {
  const double dx = (b - a) / n;    // number of steps
  double I = 0.0;

  for (int i = 0; i < n; i++) {
    const double x = a + dx * (double(i) + 0.5);
    const double y = BlackBoxFunction(x);
    const double dI = y * dx;
    I += dI;
  }

  return I;
}