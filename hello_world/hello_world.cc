/* Display "Hello, World" using multiple threads available. */

#include <stdio.h>
#include <omp.h>

int main(int argc, char const *argv[]) {

#ifdef __INTEL_COMPILER
  printf("Intel Compiler.\n");
#elif __GNUC__
  printf("GNU Compiler.\n");
#endif

#pragma omp parallel 
  {
    int ID = omp_get_thread_num();
    printf("Hello, World (%d)\n", ID);
  }
  return 0;
}
