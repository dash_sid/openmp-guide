## OpenMP Guide ##

This OpenMP repository contains self-made notes, links to available tutorials and blogs, and codes that cover topics related to vectorization (OpenMP SIMD), multithreading (OpenMP), and cluster-computing (MPI) using Intel and GNU-GCC compilers.

The topics included in this guide are OpenMP, SIMD, MPI, SSE / AVX instructions, mutexes, data and cache locality (loop fusion, tiling and mining), high-bandwidth memory (HBM), NUMA, strides, compiler-optimization, and qsub.


### Platform Configuration ###

* Fedora 31
* Intel i7-3770, 16GB DDR3
* [Intel System Studio 2020](https://software.intel.com/en-us/system-studio)
* GNU-GCC v9.2.1
* CLANG v9.0.1
* OpenMP v4.5

### Setup ###

* OpenMP installation (for clang and gcc) -- dnf install libomp.x86_64 libomp-devel.x86_64 libgomp.x86_64 openmpi.x86_64 
* Intel System Studio installation -- download the toolkit from the Intel website and follow the instructions as mentioned in the documentation and enter the license key
* To use Intel's compiler in CLI mode -- vim /etc/profile.d/intel_system_studio.sh  


```
export INTEL_SDK=/home/<user_name>/intel/system_studio_2020
export PATH=$PATH:${INTEL_SDK}/bin:${INTEL_SDK}/opencl/SDK/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${INTEL_SDK}/lib:${INTEL_SDK}/lib/intel64:${INTEL_SDK}/opencl/SDK/lib64
export INCLUDE_DIRS=$INCLUDE_DIRS:${INTEL_SDK}/include:${INTEL_SDK}/opencl/SDK/include
```

### Sources ###

* [Lawrence Livermore National Laboratory - HPC Tutorials](https://hpc.llnl.gov/training/tutorials)
* [Coursera - Fundamentals of Parallelism on Intel Architecture](https://www.coursera.org/learn/parallelism-ia)
* [Intel Vectorization Guide](https://software.intel.com/en-us/articles/vectorization-essential)
* [Intel Compiler Options for Intel SSE and Intel AVX Generation](https://software.intel.com/en-us/articles/performance-tools-for-software-developers-intel-compiler-options-for-sse-generation-and-processor-specific-optimizations)
* [Colfax Research Training](https://colfaxresearch.com/training/)
* [Intel Xeon Phi Optimization Blog - Multithreading and Parallelism](http://www.techenablement.com/intel-xeon-phi-optimization-part-1-of-3-multi-threading-and-parallel-reduction/)
* https://easyperf.net/blog/2017/10/24/Vectorization_part1
* [Strided memory access on CPUs, GPUs, and MIC](https://www.karlrupp.net/2016/02/strided-memory-access-on-cpus-gpus-and-mic/)
* [Optimization techniques for the Intel MIC architecture](https://software.intel.com/en-us/articles/optimization-techniques-for-the-intel-mic-architecture-part-1-of-3)
* [Explicit vector programming with OpenMP](http://www.hpctoday.com/hpc-labs/explicit-vector-programming-with-openmp-4-0-simd-extensions/)
* [OpenMP in a nutshell](https://www.bowdoin.edu/~ltoma/teaching/cs3225-GIS/fall17/Lectures/openmp.html)
* [Data Alignment to Assist Vectorization](https://software.intel.com/en-us/articles/data-alignment-to-assist-vectorization)
* [GCC Auto-Vectorization](https://gcc.gnu.org/projects/tree-ssa/vectorization.html)
* [GCC Vectorization reports](https://gcc.gnu.org/ml/gcc-patches/2005-01/msg01247.html)
* [Fast Numerical Programming with GCC using SIMD](https://berthub.eu/gcc-simd/index.html)